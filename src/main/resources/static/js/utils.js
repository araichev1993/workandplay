function confirmDelete(id) {
    var result = confirm("Are you sure ?")
    if (result) {
        $.ajax({
            type: "POST",
            data: {"_csrf": $('#crfToken').val()},
            url: "/campaign/delete/" + id,
            success: function (data) {
                window.location.reload()
                console.log(data)
            },
            error: function (data) {
                console.log(data)
            }
        })
    }
}
var stepsId = 1;
function addStep() {
    $.get({url: "/form"}).then(function (data) {
        $("#steps").append($(data).attr("id", ++stepsId))
    })
}
var hintsId = 1;
function addHint(element) {

    $.get({url: "/hint"}).then(function (data) {
        let a  = $(element).attr("id");
        $(element).parent().find("#hints").append($(data).attr("id","step" + a + "-" + ++hintsId))
    })
}
function addCampaign() {
    var steps = [];

    for (let child of $("#steps form")) {
        let description = $("#" + child.id + " textarea[name='description']").val()
        let confirmation = $("#" + child.id + " select[name='confirmation']").val()
        let prize = $("#" + child.id + " input[name='prize']").val()
        if(prize==""){
            alert("Не си добавил стойност на наградата")
            return
        }
        if(description==""){
            alert("Не си добавил описание на стъпката ")
            return
        }

        console.log(description)

        let hints = []
        for (let hint of $("#" + child.id + " #hints .form-group")) {
            let price = $("#" + hint.id + " input[name='price']").val()
            let hintDescription = $("#" + hint.id + " input[name='hintDescription']").val()
            let currentHint = {price: price, description: hintDescription}
            hints.push(currentHint)

            if(hintDescription==""){
                alert("Не си добавил описание на подсказката ")
                return
            }
            if(price==""){
                alert("Не си добавил цена на подсказката ")
                return
            }
        }
        var currentStep = {description: description, confirmations: confirmation, hints: hints,prize:prize}

        steps.push(currentStep);
        console.log(steps)
    }

    let campaignType = $("#campaign" + " input[name='optradio']:checked").data('multi')
    let endDate = $("#campaign" + " input[name='endDate']").val()
    let startDate = $("#campaign" + " input[name='startDate']").val()
    let description = $("#campaign" + " textarea[name='description']").val()
    let company = $("#campaign" + " select[name='company']").val()
    var token = $("input[name='_csrf']").val();

    if(startDate > endDate){
        alert("End date cannot be smaller than start date")
        return
    }
    console.log(steps.length)
    if(steps.length == 0){
        alert("You have to add at least one step")
        return
    }

    var campaign = {endDate: endDate, startDate: startDate, description: description, steps: steps,company:company,multiple:campaignType}
    var myJSON = JSON.stringify(campaign);

    let fd = new FormData()
    fd.append("_csrf", token);
    fd.append("campaign", campaign);

    $.ajax({
        type: 'POST',
        url: '/campaign/add',
        data: myJSON,
        contentType:"application/json",
       success: function (data) {
            console.log(data)
           if(data.length > 0) {
               for (let error of data) {
                   $("#errors").append($("<p>").text(error).addClass("text-danger"))
               }
           } else {
                window.location.href = "/campaign"
           }
        },
        error: function (data) {
            console.log(data)
        }
    });

}
function hideAddStepButton() {
    $("#steps").empty()
    $("#addStep").hide();
    addStep()
}
function showAddStepButton() {
    $("#addStep").show();
    $("#steps").empty()
}

function getHint(id) {

    $.ajax({
        type: "GET",
        url: "/getHint/"+id,
        success: function (data) {
            let hint = data
            if(hint.price == null){
                alert('Нямаш повече хинтове за използване')
            } else{
                var result = confirm("Сигурен ли си ? Ще ти бъдат взети "+hint.price +" парички")
                if(!hint.enoughUserMoney){
                    alert("Нямаш достатъчно парички")
                }else {
                    if (result) {
                        $.ajax({
                            type: 'GET',
                            url: '/hint/' + hint.id,
                            success: function (data) {
                                $("#hints").append($(data))
                            },
                            error: function (data) {
                                console.log(data)
                            }
                        });
                    }
                }
            }
        },
        error: function (data) {
            console.log(data)
        }
    })

}
function completeStep(id) {

    let confirmationText = $("input[name='answer']").val()
    var bindingModel ={stepId:id,confirmationText:confirmationText}
        $.ajax({
            method: 'POST',
            url: '/step/completeStep',
            data:JSON.stringify(bindingModel),
            contentType:"application/json",
            success: function (data) {
                window.location.reload()
            },
            error: function (data) {
                console.log(data)
            }
        })
}