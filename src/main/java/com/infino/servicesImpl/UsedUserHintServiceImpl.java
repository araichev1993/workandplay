package com.infino.servicesImpl;

import com.infino.entities.Hint;
import com.infino.entities.Step;
import com.infino.entities.UsedUserHint;
import com.infino.entities.User;
import com.infino.repositories.UsedUserHintRepository;
import com.infino.services.UsedUserHintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on 5/4/2017.
 */
@Service
public class UsedUserHintServiceImpl implements UsedUserHintService {

    @Autowired
    private UsedUserHintRepository usedUserHintRepository;

    @Override
    public List<UsedUserHint> findAllByHintStepAndUser(Step step, User user) {
        return this.usedUserHintRepository.findAllByHintStepAndUser(step,user);
    }

    @Override
    public void createUsedUserHint(Hint hint, User user) {
        UsedUserHint usedUserHint = new UsedUserHint();
        usedUserHint.setHint(hint);
        usedUserHint.setUser(user);
        this.usedUserHintRepository.save(usedUserHint);
    }
}
