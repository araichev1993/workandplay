package com.infino.servicesImpl;

import com.infino.entities.Hint;
import com.infino.entities.Step;
import com.infino.entities.UsedUserHint;
import com.infino.entities.User;
import com.infino.models.bindingModels.HintBindingModel;
import com.infino.models.viewModels.GameHintViewModel;
import com.infino.repositories.HintRepository;
import com.infino.services.HintService;
import com.infino.services.StepService;
import com.infino.services.UsedUserHintService;
import com.infino.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Service
public class HintServiceImpl implements HintService {
    @Autowired
    private HintRepository hintRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UsedUserHintService usedUserHintService;
    @Autowired
    private StepService stepService;


    @Override
    public void createHint(HintBindingModel hintViewModel, Step step) {
        Hint hint = new Hint();
        hint.setDescription(hintViewModel.getDescription());
        hint.setPrice(hintViewModel.getPrice());
        hint.setStep(step);
        this.hintRepository.save(hint);
    }

    @Override
    public GameHintViewModel getHint(Long stepId, String username) {
        User user = this.userService.findByUsername(username);
        Step step = this.stepService.findById(stepId);
        GameHintViewModel gameHintViewModel = new GameHintViewModel();

        List<UsedUserHint> usedHints = this.usedUserHintService.findAllByHintStepAndUser(step, user);

        Hint nextUnUsedHint = null;
        for (Hint hint : step.getHints()) {
            Boolean isUsed = false;
            for (UsedUserHint usedHint : usedHints) {
                if (hint.getId().equals(usedHint.getHint().getId())) {
                    isUsed = true;
                }
            }
            if (!isUsed) {
                nextUnUsedHint = hint;
                break;
            }
        }
        if (nextUnUsedHint != null) {
            if (user.getSum().compareTo(nextUnUsedHint.getPrice()) == 1) {
                gameHintViewModel.setEnoughUserMoney(true);
            }
            gameHintViewModel.setId(nextUnUsedHint.getId());
            gameHintViewModel.setPrice(nextUnUsedHint.getPrice());
        }
        return gameHintViewModel;
    }

    @Override
    public GameHintViewModel getHintById(Long hintId, String username) {
        Hint hint = this.hintRepository.findOne(hintId);
        User user = this.userService.findByUsername(username);

        this.usedUserHintService.createUsedUserHint(hint, user);
        GameHintViewModel gameHintViewModel = new GameHintViewModel();
        gameHintViewModel.setPrice(hint.getPrice());
        gameHintViewModel.setDescription(hint.getDescription());
        gameHintViewModel.setId(hint.getId());

        return gameHintViewModel;

    }
}
