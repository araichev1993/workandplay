package com.infino.servicesImpl;

import com.infino.entities.Confirmation;
import com.infino.entities.User;
import com.infino.entities.UserConfirmation;
import com.infino.repositories.UserConfirmationRepository;
import com.infino.services.UserConfirmationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Aleksandar on 5/4/2017.
 */
@Service
public class UserConfirmationServiceImpl implements UserConfirmationService {


    @Autowired
    private UserConfirmationRepository userConfirmationRepository;


    @Override
    public UserConfirmation findByUserAndConfirmation(User user, Confirmation confirmation) {
        return this.userConfirmationRepository.findByUserAndConfirmation(user,confirmation);
    }

    @Override
    public void update(UserConfirmation userConfirmation) {
        this.userConfirmationRepository.save(userConfirmation);
    }

    @Override
    public UserConfirmation createUserConfirmation(Confirmation confirmation, User user) {
        UserConfirmation userConfirmation  = new UserConfirmation();
        userConfirmation.setConfirmation(confirmation);
        userConfirmation.setUser(user);
        return userConfirmation;
    }
}
