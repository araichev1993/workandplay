package com.infino.servicesImpl;

import com.infino.entities.Campaign;
import com.infino.entities.User;
import com.infino.entities.UserCampaign;
import com.infino.models.viewModels.UserCampaignViewModel;
import com.infino.repositories.UserCampaignRepository;
import com.infino.services.CampaignService;
import com.infino.services.UserCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 5/5/2017.
 */
@Service
public class UserCampaignServiceImpl implements UserCampaignService {

    @Autowired
    private UserCampaignRepository userCampaignRepository;

    @Autowired
    private CampaignService campaignService;
    @Override
    public UserCampaign findByCampaignAndUser(Campaign campaign, User user) {
        return this.userCampaignRepository.findByCampaignAndUser(campaign,user);
    }

    @Override
    public UserCampaign createUserCampaign(Campaign campaign, User user) {
        UserCampaign userCampaign = new UserCampaign();
        userCampaign.setCampaign(campaign);
        userCampaign.setUser(user);
        userCampaign.setPoints(BigDecimal.ZERO);
        this.userCampaignRepository.save(userCampaign);
        return userCampaign;
    }

    @Override
    public void updateUserCampaign(UserCampaign userCampaign) {
        this.userCampaignRepository.save(userCampaign);
    }

    @Override
    public Page<UserCampaignViewModel> findByCampaignOrderByPointsDesc(Pageable pageable, Long campaignId) {
        Campaign campaign = this.campaignService.findById(campaignId);
        Page<UserCampaign> userCampaigns = this.userCampaignRepository.findByCampaignOrderByPointsDesc(pageable,campaign);

        List<UserCampaignViewModel> userCampaignViewModels  = new ArrayList<>();
        for (UserCampaign userCampaign : userCampaigns) {
            UserCampaignViewModel userCampaignViewModel = new UserCampaignViewModel();
            userCampaignViewModel.setCampaignDescription(userCampaign.getCampaign().getDescription());
            userCampaignViewModel.setCompanyName(userCampaign.getCampaign().getCompany().getName());
            userCampaignViewModel.setUsername(userCampaign.getUser().getUsername());
            userCampaignViewModel.setCampaignId(userCampaign.getCampaign().getId());
            userCampaignViewModel.setPoints(userCampaign.getPoints());
            userCampaignViewModels.add(userCampaignViewModel);

        }

        return new PageImpl<UserCampaignViewModel>(userCampaignViewModels,pageable,userCampaigns.getTotalElements());
    }
}
