package com.infino.servicesImpl;

import com.infino.entities.Company;
import com.infino.entities.User;
import com.infino.models.bindingModels.CompanyBindingModel;
import com.infino.models.viewModels.CompanyViewModel;
import com.infino.repositories.CompanyRepository;
import com.infino.services.CompanyService;
import com.infino.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 4/27/2017.
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    @Override
    public List<String> findAllCompanyNames() {
        List<String> companyNames = new ArrayList<>();

        List<Company> companies = (List<Company>) this.companyRepository.findAll();

        for (Company company : companies) {
            companyNames.add(company.getName());
        }

        return companyNames;
    }

    @Override
    public Company findByName(String name) {
        return this.companyRepository.findByName(name);
    }

    @Override
    public void createCompany(CompanyBindingModel companyBindingModel, String username) {
        Company company = new Company();
        company.setName(companyBindingModel.getName());
        User user = this.userService.findByUsername(username);
        company.setCreator(user);
        this.companyRepository.save(company);
    }

    @Override
    public Page<CompanyViewModel> findAll(Pageable pageable) {
        Page<Company> companies = this.companyRepository.findAll(pageable);
        List<CompanyViewModel> companyViewModels = new ArrayList<>();

        for (Company company : companies) {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel.setName(company.getName());
            companyViewModels.add(companyViewModel);
        }
        Page<CompanyViewModel> companyViewModelPage = new PageImpl<CompanyViewModel>(companyViewModels, pageable, companies.getTotalElements());

        return companyViewModelPage;

    }
}
