package com.infino.servicesImpl;

import com.infino.entities.PointsPrize;
import com.infino.entities.Prize;
import com.infino.repositories.PrizeRepository;
import com.infino.services.PrizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by Aleksandar on 5/5/2017.
 */
@Service
public class PrizeServiceImpl implements PrizeService {

    @Autowired
    private PrizeRepository prizeRepository;

    @Override
    public Prize createPointPrize(BigDecimal points) {
        PointsPrize pointsPrize = new PointsPrize();
        pointsPrize.setPoints(points);
        return this.prizeRepository.save(pointsPrize);
    }
}
