package com.infino.servicesImpl;

import com.infino.entities.*;
import com.infino.enums.ConfirmationLevel;
import com.infino.models.bindingModels.HintBindingModel;
import com.infino.models.bindingModels.StepBindingModel;
import com.infino.models.viewModels.GameStepViewModel;
import com.infino.repositories.StepRepository;
import com.infino.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Service
public class StepServiceImpl implements StepService {

    @Autowired
    private StepRepository stepRepository;
    @Autowired
    private HintService hintService;
    @Autowired
    private CampaignService campaignService;
    @Autowired
    private UserStepService userStepService;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserConfirmationService userConfirmationService;
    @Autowired
    private ConfirmationService confirmationService;
    @Autowired
    private PrizeService prizeService;
    @Autowired
    private UserCampaignService userCampaignService;


    @Override
    public void createStep(StepBindingModel stepViewModel, Campaign campaign) {
        Step step = new Step();
        step.setDescription(stepViewModel.getDescription());
        step.setCampaign(campaign);
        this.stepRepository.save(step);
        for (HintBindingModel hintViewModel : stepViewModel.getHints()) {
            this.hintService.createHint(hintViewModel, step);
        }
        Confirmation confirmation = this.confirmationService.createConfirmation(stepViewModel.getConfirmations(), step);
        step.setConfirmation(confirmation);
        Prize prize = this.prizeService.createPointPrize(stepViewModel.getPrize());
        step.setPrize(prize);
        this.stepRepository.save(step);

    }

    @Override
    public GameStepViewModel getNextStep(Long id, String username) {

        User user = this.userService.findByUsername(username);

        Campaign campaign = this.campaignService.findById(id);

        UserStep userStep = this.userStepService.findByUserAndStepCampaignAndIsLast(user, campaign, true);

        GameStepViewModel gameStep = new GameStepViewModel();

        UserCampaign userCampaign = this.userCampaignService.findByCampaignAndUser(campaign, user);
        if (userCampaign == null) {
            userCampaign = this.userCampaignService.createUserCampaign(campaign, user);
        }

        if (campaign.getMultiple()) {
            if (userStep != null) {
                if (userStep.getConfirmationLevel().equals(ConfirmationLevel.NEW)) {
                    gameStep.setDescription(userStep.getStep().getDescription());
                    gameStep.setConfirmationLevel(ConfirmationLevel.NEW);
                    gameStep.setId(userStep.getStep().getId());
                } else if (userStep.getConfirmationLevel().equals(ConfirmationLevel.WAITING_CONFIRMATION)) {
                    gameStep.setDescription(userStep.getStep().getDescription());
                    gameStep.setConfirmationLevel(ConfirmationLevel.WAITING_CONFIRMATION);
                } else if (userStep.getConfirmationLevel().equals(ConfirmationLevel.CONFIRMED)) {
                    userStep.setLast(false);
                    this.userStepService.updateUserStep(userStep);
                    Step nexStep = null;
                    for (Step step : userStep.getStep().getCampaign().getSteps()) {
                        boolean isUsed = false;
                        for (UserStep userStep1 : this.userStepService.findAllByStepCampaignAndUser(userStep.getStep().getCampaign(), user)) {
                            if (step.getId().equals(userStep1.getStep().getId())) {
                                isUsed = true;
                                break;
                            }
                        }
                        if (!isUsed) {
                            nexStep = step;
                            break;
                        }
                    }
                    if (nexStep != null) {
                        this.userStepService.createUserStep(nexStep, userStep.getUser());
                        gameStep.setDescription(nexStep.getDescription());
                        gameStep.setId(nexStep.getId());
                    }
                }
            } else {
                Step nexStep = null;
                for (Step step : campaign.getSteps()) {
                    boolean isUsed = false;
                    for (UserStep userStep1 : this.userStepService.findAllByStepCampaignAndUser(campaign, user)) {
                        if (step.getId().equals(userStep1.getStep().getId())) {
                            isUsed = true;
                            break;
                        }
                    }
                    if (!isUsed) {
                        nexStep = step;
                        break;
                    }
                }
                if (nexStep != null) {
                    this.userStepService.createUserStep(nexStep, user);
                    gameStep.setDescription(nexStep.getDescription());
                    gameStep.setId(nexStep.getId());
                }

            }
        } else {
            if (userStep != null) {
                if (userStep.getConfirmationLevel().equals(ConfirmationLevel.NEW)) {
                    gameStep.setDescription(userStep.getStep().getDescription());
                    gameStep.setId(userStep.getStep().getId());
                } else if (userStep.getConfirmationLevel().equals(ConfirmationLevel.WAITING_CONFIRMATION)) {
                    gameStep.setDescription(userStep.getStep().getDescription());
                    gameStep.setConfirmationLevel(ConfirmationLevel.WAITING_CONFIRMATION);
                } else if (userStep.getConfirmationLevel().equals(ConfirmationLevel.CONFIRMED)) {
                    userStep.setLast(false);
                    this.userStepService.createUserStep(userStep.getStep(), userStep.getUser());
                    gameStep.setDescription(userStep.getStep().getDescription());
                    gameStep.setId(userStep.getStep().getId());
                }
            } else {
                List<Step> allSteps = this.stepRepository.findAllByCampaign(campaign);
                if (allSteps.size() > 0) {
                    Step nexStep = allSteps.get(0);
                    this.userStepService.createUserStep(nexStep, user);
                    gameStep.setDescription(nexStep.getDescription());
                    gameStep.setId(nexStep.getId());
                }

            }
        }
        return gameStep;
    }

    @Override
    public GameStepViewModel completeStep(Long id, String username, String confirmationText) {
        Step step = this.stepRepository.findOne(id);
        User user = this.userService.findByUsername(username);
        GameStepViewModel gameStepViewModel = new GameStepViewModel();
        UserStep userStep = this.userStepService.findByUserAndStepCampaignAndIsLast(user, step.getCampaign(), true);

        if (step.getConfirmation() instanceof BarcodeConfirmation) {
            if (((BarcodeConfirmation) step.getConfirmation()).getBarcode().equals(confirmationText)) {
                gameStepViewModel.setConfirmationLevel(ConfirmationLevel.CONFIRMED);
                userStep.setConfirmationLevel(ConfirmationLevel.CONFIRMED);
                if (step.getPrize() instanceof MoneyPrize) {
                    user.setSum(user.getSum().add(((MoneyPrize) step.getPrize()).getMoney()));
                } else {
                    user.setPoints(user.getPoints().add(((PointsPrize) step.getPrize()).getPoints()));
                    UserCampaign userCampaign = this.userCampaignService.findByCampaignAndUser(userStep.getStep().getCampaign(), user);
                    userCampaign.setPoints(userCampaign.getPoints().add(((PointsPrize) step.getPrize()).getPoints()));
                    this.userCampaignService.updateUserCampaign(userCampaign);
                }
            }
        } else {
            userStep.getUserConfirmation().setConfirtmationText(confirmationText);
            gameStepViewModel.setConfirmationLevel(ConfirmationLevel.WAITING_CONFIRMATION);
            userStep.setConfirmationLevel(ConfirmationLevel.WAITING_CONFIRMATION);

        }
        gameStepViewModel.setDescription(step.getDescription());
        gameStepViewModel.setId(step.getId());

        this.userStepService.updateUserStep(userStep);
        this.userService.update(user);

        return gameStepViewModel;
    }

    @Override
    public Step findById(Long id) {
        return this.stepRepository.findOne(id);
    }

}
