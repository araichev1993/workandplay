package com.infino.servicesImpl;

import com.infino.entities.*;
import com.infino.models.bindingModels.CampaignBindingModel;
import com.infino.models.bindingModels.StepBindingModel;
import com.infino.models.viewModels.*;
import com.infino.repositories.CampaignRepository;
import com.infino.repositories.CompanyRepository;
import com.infino.services.CampaignService;
import com.infino.services.StepService;
import com.infino.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 4/28/2017.
 */
@Service
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private StepService stepService;

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Page<CampaignViewModel> findAllCampaign(Pageable pageable) {
        Page<Campaign> campaigns = this.campaignRepository.findAllByIsDeletedIsFalse(pageable);
        List<CampaignViewModel> campaignBindingModels = new ArrayList<>();

        for (Campaign campaign : campaigns) {
            CampaignViewModel campaignBindingModel = this.modelMapper.map(campaign, CampaignViewModel.class);
            campaignBindingModels.add(campaignBindingModel);
        }

        Page<CampaignViewModel> campaignModels = new PageImpl<>(campaignBindingModels, pageable, campaigns.getTotalElements());

        return campaignModels;
    }

    @Override
    public Page<CampaignViewModel> findAllUserCampaigns(String username, Pageable pageable) {
        User user = this.userService.findByUsername(username);

        Page<Campaign> campaigns = this.campaignRepository.findAllByCompanyAndIsDeletedIsFalse(user.getCompany(), pageable);
        List<CampaignViewModel> campaignBindingModels = new ArrayList<>();
        for (Campaign campaign : campaigns) {
            CampaignViewModel campaignBindingModel = this.modelMapper.map(campaign,CampaignViewModel.class);
            campaignBindingModels.add(campaignBindingModel);

        }
        Page<CampaignViewModel> campaignModels = new PageImpl<>(campaignBindingModels, pageable, campaigns.getTotalElements());
        return campaignModels;
    }

    @Override
    public void deleteCampaign(Long id) {
        Campaign campaign = this.campaignRepository.findOne(id);
        campaign.setIsDeleted(true);
        this.campaignRepository.save(campaign);
    }

    @Override
    public void createCampaign(CampaignBindingModel campaignViewModel) {
        Campaign campaign = new Campaign();
        campaign.setMultiple(campaignViewModel.getMultiple());
        campaign.setDescription(campaignViewModel.getDescription());
        campaign.setEndDate(campaignViewModel.getEndDate());
        campaign.setStartDate(campaignViewModel.getStartDate());
        Company company = this.companyRepository.findByName(campaignViewModel.getCompany());
        campaign.setCompany(company);
        this.campaignRepository.save(campaign);
        for (StepBindingModel stepViewModel : campaignViewModel.getSteps()) {
            this.stepService.createStep(stepViewModel, campaign);
        }
    }

    @Override
    public CampaignEditViewModel editCampaign(Long id) {
        Campaign campaign = this.campaignRepository.findOne(id);
        CampaignEditViewModel campaignEditViewModel = new CampaignEditViewModel();
        campaignEditViewModel.setId(campaign.getId());
        campaignEditViewModel.setCompany(campaign.getCompany().getName());
        campaignEditViewModel.setDescription(campaign.getDescription());
        campaignEditViewModel.setEndDate(campaign.getEndDate());
        campaignEditViewModel.setStartDate(campaign.getStartDate());
        List<StepEditViewModel> steps = new ArrayList<>();
        for (Step step : campaign.getSteps()) {
            StepEditViewModel stepViewModel = new StepEditViewModel();
            stepViewModel.setDescription(step.getDescription());
            stepViewModel.setId(step.getId());

            ConfirmationEditViewModel confirmationViewModel = new ConfirmationEditViewModel();
            confirmationViewModel.setId(step.getConfirmation().getId());
            stepViewModel.setConfirmation(confirmationViewModel);
            List<HintEditViewModel> hintViewModels = new ArrayList<>();
            for (Hint hint : step.getHints()) {
                HintEditViewModel hintViewModel = new HintEditViewModel();
                hintViewModel.setDescription(hint.getDescription());
                hintViewModel.setPrice(hint.getPrice());
                hintViewModel.setId(hint.getId());
                hintViewModels.add(hintViewModel);
            }
            stepViewModel.setHints(hintViewModels);
            steps.add(stepViewModel);
        }
        campaignEditViewModel.setSteps(steps);

        return campaignEditViewModel;
    }

    @Override
    public Campaign findById(Long id) {
        return this.campaignRepository.findOne(id);
    }
}
