package com.infino.servicesImpl;

import com.infino.entities.BarcodeConfirmation;
import com.infino.entities.Confirmation;
import com.infino.entities.ModeratorConfirmation;
import com.infino.entities.Step;
import com.infino.repositories.ConfirmationRepository;
import com.infino.services.ConfirmationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Service
public class ConfirmationServiceImpl implements ConfirmationService {

    @Autowired
    private ConfirmationRepository confirmationRepository;

    @Override
    public Confirmation createConfirmation(String confirmation, Step step) {
        Confirmation newConfirmation = null;
        if (confirmation.toUpperCase().equals("BARCODE_CONFIRMATION")) {
            newConfirmation = new BarcodeConfirmation();
            newConfirmation.setStep(step);
            ((BarcodeConfirmation) newConfirmation).setBarcode("barcode");
            this.confirmationRepository.save(newConfirmation);
        }
        if (confirmation.toUpperCase().equals("MODERATOR_CONFIRMATION")) {
            newConfirmation = new ModeratorConfirmation();
            newConfirmation.setStep(step);
            this.confirmationRepository.save(newConfirmation);

        }
        return newConfirmation;
    }
}
