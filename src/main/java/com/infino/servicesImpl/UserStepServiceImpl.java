package com.infino.servicesImpl;

import com.infino.entities.*;
import com.infino.enums.ConfirmationLevel;
import com.infino.models.viewModels.UserStepViewModel;
import com.infino.repositories.UserStepRepository;
import com.infino.services.UserCampaignService;
import com.infino.services.UserConfirmationService;
import com.infino.services.UserService;
import com.infino.services.UserStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 5/3/2017.
 */
@Service
public class UserStepServiceImpl implements UserStepService {

    @Autowired
    private UserStepRepository userStepRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private UserConfirmationService userConfirmationService;
    @Autowired
    private UserCampaignService userCampaignService;

    @Override
    public UserStep findByUserAndStepCampaignAndIsLast(User user,Campaign campaign,Boolean isLast) {
        return this.userStepRepository.findByUserAndStepCampaignAndIsLast(user,campaign,isLast);
    }

    @Override
    public void updateUserStep(UserStep userStep) {
        this.userStepRepository.save(userStep);
    }

    @Override
    public void createUserStep(Step step, User user) {
        UserStep newUserStep = new UserStep();
        newUserStep.setLast(true);
        newUserStep.setStep(step);
        newUserStep.setUser(user);
        newUserStep.setConfirmationLevel(ConfirmationLevel.NEW);
        UserConfirmation userConfirmation = this.userConfirmationService.createUserConfirmation(step.getConfirmation(),user);
        newUserStep.setUserConfirmation(userConfirmation);
        this.userStepRepository.save(newUserStep);
    }

    @Override
    public List<UserStep> findAllByStepCampaignAndUser(Campaign campaign,User user) {
        return this.userStepRepository.findAllByStepCampaignAndUser(campaign,user);
    }

    @Override
    public Page<UserStepViewModel> findByUserCompanyAndAndConfirmationLevel(Pageable pageable, String username) {
        User user = this.userService.findByUsername(username);
        Page<UserStep> steps = this.userStepRepository.findByUserCompanyAndAndConfirmationLevel(pageable,user.getCompany(),ConfirmationLevel.WAITING_CONFIRMATION);

        List<UserStepViewModel> userStepViewModels = new ArrayList<>();

        for (UserStep step : steps) {
            if(step.getUser() != user) {
                UserStepViewModel userStepViewModel = new UserStepViewModel();
                userStepViewModel.setId(step.getId());
                userStepViewModel.setConfirmationText(step.getUserConfirmation().getConfirtmationText());
                userStepViewModel.setStepDescription(step.getStep().getDescription());
                userStepViewModel.setUsername(step.getUser().getUsername());
                userStepViewModels.add(userStepViewModel);
            }
        }
        Page<UserStepViewModel> userStepViewModels1 = new PageImpl<UserStepViewModel>(userStepViewModels,pageable,steps.getTotalElements());
        return userStepViewModels1;
    }

    @Override
    public void updateUserStep(String type, Long userStepId) {
        UserStep userStep = this.userStepRepository.findOne(userStepId);
        if(type.equals("confirmed")){
            userStep.setConfirmationLevel(ConfirmationLevel.CONFIRMED);
            User user = userStep.getUser();
            user.setPoints(user.getPoints().add(((PointsPrize)userStep.getStep().getPrize()).getPoints()));
            this.userService.update(user);
            UserCampaign userCampaign = this.userCampaignService.findByCampaignAndUser(userStep.getStep().getCampaign(),user);
            userCampaign.setPoints(userCampaign.getPoints().add(((PointsPrize) userStep.getStep().getPrize()).getPoints()));
            this.userCampaignService.updateUserCampaign(userCampaign);
        }else if(type.equals("denied")){
            userStep.setConfirmationLevel(ConfirmationLevel.CONFIRMED);
            this.userStepRepository.save(userStep);
        }
    }

}
