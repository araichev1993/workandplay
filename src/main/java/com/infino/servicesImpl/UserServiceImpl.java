package com.infino.servicesImpl;

import com.infino.entities.Company;
import com.infino.entities.User;
import com.infino.models.bindingModels.RegistrationModel;
import com.infino.models.viewModels.UserProfilModel;
import com.infino.repositories.UserRepository;
import com.infino.repositories.CompanyRepository;
import com.infino.services.CompanyService;
import com.infino.services.RoleService;
import com.infino.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Aleksandar on 4/17/2017.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findOneByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid Credentials");
        }
        return user;
    }

    @Override
    public void register(RegistrationModel registrationModel) {
        User user = new User();
        user.setUsername(registrationModel.getUsername());
        user.setImageURL(registrationModel.getImageURL());
        user.addRole(this.roleService.findByAuthority("ROLE_USER"));
        if(registrationModel.getCompanyName() != null){
            Company company = this.companyService.findByName(registrationModel.getCompanyName());
            user.setCompany(company);
        }
        String password = this.bCryptPasswordEncoder.encode(registrationModel.getPassword());
        user.setPassword(password);
        this.userRepository.save(user);
    }

    @Override
    public void update(User user) {
        this.userRepository.save(user);
    }

    @Override
    public UserProfilModel getProfilByName(String name) {
        User user = this.userRepository.findOneByUsername(name);
        UserProfilModel userProfilModel = this.modelMapper.map(user,UserProfilModel.class);
        if(user.getAuthorities().size() > 0){
            userProfilModel.setRole(user.getAuthorities().iterator().next().getAuthority());
        }
        return  userProfilModel;
    }

    @Override
    public User findByUsername(String username) {
        return this.userRepository.findOneByUsername(username);
    }
}
