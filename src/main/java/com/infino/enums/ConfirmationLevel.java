package com.infino.enums;

/**
 * Created by Aleksandar on 5/4/2017.
 */
public enum ConfirmationLevel {
    NEW,WAITING_CONFIRMATION,CONFIRMED
}
