package com.infino.enums;

/**
 * Created by Aleksandar on 4/30/2017.
 */
public enum ConfirmationType {
    BARCODE_CONFIRMATION,MODERATOR_CONFIRMATION
}
