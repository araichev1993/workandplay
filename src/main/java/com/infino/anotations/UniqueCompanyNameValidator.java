package com.infino.anotations;

import com.infino.entities.Company;
import com.infino.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Aleksandar on 5/2/2017.
 */
public class UniqueCompanyNameValidator implements ConstraintValidator<UniqueCompanyName,Object> {

    @Autowired
    private CompanyService companyService;

    @Override
    public void initialize(UniqueCompanyName unique) {

    }

    @Override
    public boolean isValid(Object companyName, ConstraintValidatorContext constraintValidatorContext) {
        if(companyName instanceof String){
            Company company = this.companyService.findByName((String) companyName);
            if(company != null){
                return false;
            }
        }
        return true;
    }
}
