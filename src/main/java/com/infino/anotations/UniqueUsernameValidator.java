package com.infino.anotations;

import com.infino.entities.User;
import com.infino.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Aleksandar on 5/2/2017.
 */
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, Object> {

    @Autowired
    private UserService userService;

    @Override
    public void initialize(UniqueUsername unique) {

    }

    @Override
    public boolean isValid(Object username, ConstraintValidatorContext constraintValidatorContext) {
        if(username instanceof String){
            User user = this.userService.findByUsername((String) username);
            if(user != null){
                return false;
            }
        }
        return true;
    }
}
