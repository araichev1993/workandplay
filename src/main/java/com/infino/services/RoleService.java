package com.infino.services;

import com.infino.entities.Role;

/**
 * Created by Aleksandar on 5/8/2017.
 */
public interface RoleService {

    Role findByAuthority(String authority);
}
