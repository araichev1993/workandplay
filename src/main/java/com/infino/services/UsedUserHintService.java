package com.infino.services;

import com.infino.entities.Hint;
import com.infino.entities.Step;
import com.infino.entities.UsedUserHint;
import com.infino.entities.User;

import java.util.List;

/**
 * Created by Aleksandar on 5/4/2017.
 */
public interface UsedUserHintService {


    List<UsedUserHint> findAllByHintStepAndUser(Step step, User user);


    void createUsedUserHint(Hint hint,User user);
}
