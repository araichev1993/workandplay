package com.infino.services;

import com.infino.entities.Company;
import com.infino.models.bindingModels.CompanyBindingModel;
import com.infino.models.viewModels.CompanyViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Aleksandar on 4/27/2017.
 */
public interface CompanyService {

    List<String> findAllCompanyNames();

    Company findByName(String name);

    void createCompany(CompanyBindingModel companyBindingModel,String username);

    Page<CompanyViewModel> findAll(Pageable pageable);
}
