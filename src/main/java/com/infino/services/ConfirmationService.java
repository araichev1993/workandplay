package com.infino.services;

import com.infino.entities.Confirmation;
import com.infino.entities.Step;

/**
 * Created by Aleksandar on 5/1/2017.
 */
public interface ConfirmationService {

    Confirmation createConfirmation(String confirmations, Step step);
}
