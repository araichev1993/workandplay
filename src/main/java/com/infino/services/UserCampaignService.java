package com.infino.services;

import com.infino.entities.Campaign;
import com.infino.entities.User;
import com.infino.entities.UserCampaign;
import com.infino.models.viewModels.UserCampaignViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Aleksandar on 5/5/2017.
 */
public interface UserCampaignService {

    UserCampaign findByCampaignAndUser(Campaign campaign,User user);

    UserCampaign createUserCampaign(Campaign campaign , User user);

    void updateUserCampaign(UserCampaign userCampaign);

    Page<UserCampaignViewModel> findByCampaignOrderByPointsDesc(Pageable pageable,Long campaignId);


}
