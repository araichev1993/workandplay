package com.infino.services;

import com.infino.entities.User;
import com.infino.models.bindingModels.RegistrationModel;
import com.infino.models.viewModels.UserProfilModel;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Aleksandar on 4/26/2017.
 */
public interface UserService extends UserDetailsService {

    void register(RegistrationModel registrationModel);

    void update (User user);

    UserProfilModel getProfilByName(String name);

    User findByUsername(String username);
}
