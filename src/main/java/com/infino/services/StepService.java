package com.infino.services;

import com.infino.entities.Campaign;
import com.infino.entities.Step;
import com.infino.models.bindingModels.StepBindingModel;
import com.infino.models.viewModels.GameStepViewModel;

import java.util.List;

/**
 * Created by Aleksandar on 5/1/2017.
 */
public interface StepService {

    void createStep(StepBindingModel stepViewModel, Campaign campaign);

    GameStepViewModel getNextStep(Long id,String username);

    GameStepViewModel completeStep(Long id,String username,String answer);

    Step findById(Long id);
}
