package com.infino.services;

import com.infino.entities.Confirmation;
import com.infino.entities.User;
import com.infino.entities.UserConfirmation;

/**
 * Created by Aleksandar on 5/4/2017.
 */
public interface UserConfirmationService {

    UserConfirmation findByUserAndConfirmation(User user, Confirmation confirmation);

    void update(UserConfirmation userConfirmation);

    UserConfirmation createUserConfirmation(Confirmation confirmation,User user);
}
