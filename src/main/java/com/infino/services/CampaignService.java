package com.infino.services;

import com.infino.entities.Campaign;
import com.infino.models.bindingModels.CampaignBindingModel;
import com.infino.models.viewModels.CampaignEditViewModel;
import com.infino.models.viewModels.CampaignViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Aleksandar on 4/28/2017.
 */
public interface CampaignService {

    Page<CampaignViewModel> findAllCampaign(Pageable pageable);

    Page<CampaignViewModel> findAllUserCampaigns(String username, Pageable pageable);

    void deleteCampaign(Long id);

    void createCampaign(CampaignBindingModel campaignViewModel);

    CampaignEditViewModel editCampaign(Long id);

    Campaign findById(Long id);
}
