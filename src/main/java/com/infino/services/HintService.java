package com.infino.services;

import com.infino.entities.Step;
import com.infino.models.bindingModels.HintBindingModel;
import com.infino.models.viewModels.GameHintViewModel;

/**
 * Created by Aleksandar on 5/1/2017.
 */
public interface HintService {

    void createHint(HintBindingModel hintViewModel, Step step);

    GameHintViewModel getHint(Long stepId,String username);

    GameHintViewModel getHintById(Long hintId,String username);

}
