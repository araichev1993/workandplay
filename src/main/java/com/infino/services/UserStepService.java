package com.infino.services;

import com.infino.entities.*;
import com.infino.enums.ConfirmationLevel;
import com.infino.models.viewModels.UserStepViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Aleksandar on 5/3/2017.
 */
public interface UserStepService {

    UserStep findByUserAndStepCampaignAndIsLast(User user,Campaign campaign,Boolean isLast);

    void updateUserStep(UserStep userStep);

    void createUserStep(Step step,User user);

    List<UserStep> findAllByStepCampaignAndUser(Campaign campaign,User user);

    Page<UserStepViewModel> findByUserCompanyAndAndConfirmationLevel(Pageable pageable,String username);

    void updateUserStep(String type,Long userStepId);
}
