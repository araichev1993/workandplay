package com.infino.services;

import com.infino.entities.Prize;

import java.math.BigDecimal;

/**
 * Created by Aleksandar on 5/5/2017.
 */
public interface PrizeService {
    Prize createPointPrize(BigDecimal points);
}
