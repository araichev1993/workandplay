package com.infino.controllers;

import com.infino.models.bindingModels.RegistrationModel;
import com.infino.models.viewModels.UserProfilModel;
import com.infino.services.CompanyService;
import com.infino.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Aleksandar on 4/17/2017.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @ModelAttribute("companies")
    public List<String> getCompanyNames() {

        return this.companyService.findAllCompanyNames();
    }


    @GetMapping("/register")
    public String getRegisterPage(@ModelAttribute RegistrationModel registrationModel, Model model) {
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute RegistrationModel registrationModel, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "register";
        }

        this.userService.register(registrationModel);

        return "redirect:/login";
    }

    @GetMapping("/profil")
    public String getProfilPage(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username

        UserProfilModel userProfileModel = this.userService.getProfilByName(name);
        model.addAttribute("profile", userProfileModel);
        return "profile";
    }


    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }
}
