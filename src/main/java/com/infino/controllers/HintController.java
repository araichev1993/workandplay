package com.infino.controllers;

import com.infino.models.viewModels.GameHintViewModel;
import com.infino.services.HintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Aleksandar on 5/4/2017.
 */
@Controller
public class HintController {

    @Autowired
    private HintService hintService;

    @GetMapping("/getHint/{stepId}")
    @ResponseBody
    public GameHintViewModel getHintPrice(@PathVariable Long stepId) {
        String username = getUserUsername();

        GameHintViewModel gameHintViewModel = this.hintService.getHint(stepId, username);

        return gameHintViewModel;
    }

    @GetMapping("/hint/{hintId}")
    public String getHintPage(Model model, @PathVariable Long hintId) {
        String username = getUserUsername();

        GameHintViewModel gameHintViewModel = this.hintService.getHintById(hintId, username);

        model.addAttribute("hint", gameHintViewModel);

        return "game-hint-form";
    }

    private String getUserUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return username;
    }
}
