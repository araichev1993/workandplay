package com.infino.controllers;

import com.infino.models.bindingModels.CompanyBindingModel;
import com.infino.models.viewModels.CompanyViewModel;
import com.infino.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * Created by Aleksandar on 5/2/2017.
 */
@Controller
@RequestMapping(value = "/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping(value = "/add")
    public String getAddPage(@ModelAttribute CompanyBindingModel companyBindingModel) {
        return "add-company";
    }

    @PostMapping(value = "/add")
    public String addCompany(@Valid @ModelAttribute CompanyBindingModel companyBindingModel, BindingResult result) {

        if (result.hasErrors()) {
            return "add-company";
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        this.companyService.createCompany(companyBindingModel, username);

        return "redirect:/";
    }

    @GetMapping(value = "/all")
    public String getCampaignsPage(Model model, @PageableDefault(size = 9) Pageable pageable) {

        Page<CompanyViewModel> companyViewModelPage = this.companyService.findAll(pageable);
        model.addAttribute("companies", companyViewModelPage);

        return "companies-form";
    }
}
