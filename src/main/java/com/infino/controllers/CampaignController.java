package com.infino.controllers;

import com.infino.enums.ConfirmationType;
import com.infino.models.bindingModels.CampaignBindingModel;
import com.infino.models.viewModels.CampaignEditViewModel;
import com.infino.models.viewModels.CampaignViewModel;
import com.infino.services.CampaignService;
import com.infino.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 4/27/2017.
 */
@Controller
@RequestMapping("/campaign")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private CompanyService companyService;

    @ModelAttribute("confirmations")
    public ConfirmationType[] getMagnitudes() {
        return ConfirmationType.values();
    }

    @ModelAttribute("companies")
    public List<String> getCompanyNames() {

        return this.companyService.findAllCompanyNames();
    }

    @RequestMapping("")
    public String getCampaignPage(Model model, @PageableDefault(size = 5) Pageable pageable) {
        Page<CampaignViewModel> campaignBindingModelList = null;

        Boolean isUser = false;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority grantedAuthority : auth.getAuthorities()) {
            if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
                isUser = true;
            }
        }
        if (isUser) {
            String currentUserUsername = auth.getName();
            campaignBindingModelList = this.campaignService.findAllUserCampaigns(currentUserUsername, pageable);
        } else {
            campaignBindingModelList = this.campaignService.findAllCampaign(pageable);
        }
        model.addAttribute("campaigns", campaignBindingModelList);


        return "campaigns-form";
    }

    @GetMapping(path = "/add")
    public String getAddPage(Model model, @ModelAttribute CampaignBindingModel campaignBindingModel) {
        return "add-campaign";
    }

    @GetMapping(path = "/edit/{id}")
    public String getEditPage(Model model, @PathVariable Long id) {

        CampaignEditViewModel campaign = this.campaignService.editCampaign(id);
        model.addAttribute("campaign", campaign);

        return "edit-campaign";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCampaign(@PathVariable("id") Long id) {
        this.campaignService.deleteCampaign(id);

        return "a";
    }

    @PostMapping(path = "/add")
    public ResponseEntity<List<String>> addCampaign(@RequestBody CampaignBindingModel campaignBindingModel, BindingResult result, Model model) {

        List<String> errors = new ArrayList<>();
        if (campaignBindingModel.getEndDate().before(campaignBindingModel.getStartDate())) {
            errors.add("End date must be after start date");
        }
        if (campaignBindingModel.getDescription().equals("")) {
            errors.add("Description should not be blank");
        }


        this.campaignService.createCampaign(campaignBindingModel);
        return new ResponseEntity<>(errors, HttpStatus.OK);
    }
}
