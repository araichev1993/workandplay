package com.infino.controllers;

import com.infino.enums.ConfirmationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {


    @ModelAttribute("confirmations")
    public ConfirmationType[] getMagnitudes() {
        return ConfirmationType.values();
    }

    @GetMapping("/")
    public String getHomePage() {
        return "home";
    }


    @GetMapping("/form")
    public String getForm() {
        return "/fragments/parts :: add-step";
    }

    @GetMapping("/hint")
    public String getHint() {
        return "/fragments/parts :: add-hint";
    }

}
