package com.infino.controllers;

import com.infino.entities.Step;
import com.infino.enums.ConfirmationLevel;
import com.infino.models.bindingModels.GameConfirmationBindingModel;
import com.infino.models.viewModels.GameStepViewModel;
import com.infino.services.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Aleksandar on 5/3/2017.
 */
@Controller
@RequestMapping("/step")
public class StepController {


    @Autowired
    private StepService stepService;

    @GetMapping(value = "/{campaignId}")
    public String getStepPage(Model model, @PathVariable Long campaignId) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        GameStepViewModel gameStepViewModel = this.stepService.getNextStep(campaignId, username);
        model.addAttribute("step", gameStepViewModel);
        if (gameStepViewModel.getDescription() == null) {
            return "end-campaign-form";
        }

        return "game-step-form";
    }

    @PostMapping(value = "/completeStep")
    public String completeStep(Model model, @RequestBody GameConfirmationBindingModel bindingModel) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Step step = this.stepService.findById(bindingModel.getStepId());

        GameStepViewModel gameStepViewModel = this.stepService.completeStep(bindingModel.getStepId(), username, bindingModel.getConfirmationText());

        if (gameStepViewModel.getConfirmationLevel().equals(ConfirmationLevel.NEW)) {
            model.addAttribute("step", gameStepViewModel);
            return "game-step-form";
        }

        return "redirect:/step/" + step.getCampaign().getId();
    }
}
