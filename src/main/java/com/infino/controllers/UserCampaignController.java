package com.infino.controllers;

import com.infino.entities.Campaign;
import com.infino.models.viewModels.UserCampaignViewModel;
import com.infino.services.CampaignService;
import com.infino.services.UserCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by Aleksandar on 5/5/2017.
 */
@Controller
public class UserCampaignController {

    @Autowired
    private UserCampaignService userCampaignService;
    @Autowired
    private CampaignService campaignService;

    @GetMapping("/leaderboard/{campaignId}")
    public String getUserCampaignsPage(Model model, Pageable pageable, @PathVariable Long campaignId) {

        Page<UserCampaignViewModel> userCampaignViewModels = this.userCampaignService.findByCampaignOrderByPointsDesc(pageable, campaignId);
        model.addAttribute("users", userCampaignViewModels);
        model.addAttribute("campaignId", campaignId);

        return "leaderboard-form";

    }
}
