package com.infino.controllers;

import com.infino.models.viewModels.UserStepViewModel;
import com.infino.services.UserStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Aleksandar on 5/5/2017.
 */
@Controller
@RequestMapping("/userStep")
public class UserStepController {

    @Autowired
    private UserStepService userStepService;

    @GetMapping("/moderator")
    public String getAllUsersStepByCompany(Model model, Pageable pageable) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName(); //get logged in username

        Page<UserStepViewModel> steps = this.userStepService.findByUserCompanyAndAndConfirmationLevel(pageable, username);
        model.addAttribute("steps", steps);

        return "waiting_confirmation-userStep-form";
    }

    @GetMapping("/{moderatorConfirmationType}/{userStepId}")
    public String moderatorConfirmation(Model model, @PathVariable String moderatorConfirmationType, @PathVariable Long userStepId) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName(); //get logged in username

        this.userStepService.updateUserStep(moderatorConfirmationType, userStepId);
        return "redirect:/userStep/moderator";
    }
}
