package com.infino.controllers;

import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;

/**
 * Created by Aleksandar on 5/10/2017.
 */
@ControllerAdvice
public class GlobalExceptionHandling {


    @ExceptionHandler(Exception.class)
    public String handleGlobalExceptions(){

        return "exceptions/custom-error";

    }

    @ExceptionHandler(Throwable.class)
    public String handleAnyException() {
        return "exceptions/404";
    }
}
