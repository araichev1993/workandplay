package com.infino.repositories;

import com.infino.entities.Campaign;
import com.infino.entities.Company;
import com.infino.entities.User;
import com.infino.entities.UserStep;
import com.infino.enums.ConfirmationLevel;
import com.infino.models.viewModels.UserStepViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aleksandar on 5/3/2017.
 */
@Repository
public interface UserStepRepository extends PagingAndSortingRepository<UserStep,Long> {

    UserStep findByUserAndStepCampaignAndIsLast(User user,Campaign campaign,Boolean isLast);

    List<UserStep> findAllByStepCampaignAndUser(Campaign campaign,User user);

    Page<UserStep> findByUserCompanyAndAndConfirmationLevel(Pageable pageable,Company company, ConfirmationLevel confirmationLevel);
}
