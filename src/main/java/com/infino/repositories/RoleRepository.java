package com.infino.repositories;

import com.infino.entities.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Aleksandar on 5/8/2017.
 */
public interface RoleRepository extends CrudRepository<Role,Long> {

    Role findByAuthority(String authority);
}
