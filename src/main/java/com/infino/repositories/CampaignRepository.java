package com.infino.repositories;

import com.infino.entities.Campaign;
import com.infino.entities.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aleksandar on 4/28/2017.
 */
@Repository
public interface CampaignRepository extends PagingAndSortingRepository<Campaign, Long> {

    Page<Campaign> findAllByCompanyAndIsDeletedIsFalse(Company company, Pageable page);

    Page<Campaign> findAllByIsDeletedIsFalse(Pageable pageable);

}
