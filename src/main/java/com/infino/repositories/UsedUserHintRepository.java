package com.infino.repositories;

import com.infino.entities.Hint;
import com.infino.entities.Step;
import com.infino.entities.UsedUserHint;
import com.infino.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Aleksandar on 5/4/2017.
 */
public interface UsedUserHintRepository extends CrudRepository<UsedUserHint,Long> {

    List<UsedUserHint> findAllByHintStepAndUser(Step step, User user);
}
