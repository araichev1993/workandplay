package com.infino.repositories;

import com.infino.entities.Campaign;
import com.infino.entities.User;
import com.infino.entities.UserCampaign;
import com.infino.models.viewModels.UserCampaignViewModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Aleksandar on 5/5/2017.
 */
@Repository
public interface UserCampaignRepository extends PagingAndSortingRepository<UserCampaign,Long> {

    UserCampaign findByCampaignAndUser(Campaign campaign,User user);

    Page<UserCampaign> findByCampaignOrderByPointsDesc(Pageable pageable,Campaign campaign);
}
