package com.infino.repositories;

import com.infino.entities.Hint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Repository
public interface HintRepository extends CrudRepository<Hint,Long> {
}
