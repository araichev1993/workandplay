package com.infino.repositories;

import com.infino.entities.Campaign;
import com.infino.entities.Step;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Repository
public interface StepRepository extends CrudRepository<Step,Long> {

    List<Step> findAllByCampaign(Campaign campaign);
}
