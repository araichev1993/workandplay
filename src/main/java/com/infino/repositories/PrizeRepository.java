package com.infino.repositories;

import com.infino.entities.Prize;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Aleksandar on 5/5/2017.
 */
public interface PrizeRepository extends CrudRepository<Prize,Long> {
}
