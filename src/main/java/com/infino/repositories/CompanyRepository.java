package com.infino.repositories;

import com.infino.entities.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * Created by Aleksandar on 4/27/2017.
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company,Long> {

    Company findByName(String name);

    Page<Company> findAll(Pageable pageable);
}
