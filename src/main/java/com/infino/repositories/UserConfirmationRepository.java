package com.infino.repositories;

import com.infino.entities.Confirmation;
import com.infino.entities.User;
import com.infino.entities.UserConfirmation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Aleksandar on 5/4/2017.
 */
@Repository
public interface UserConfirmationRepository extends CrudRepository<UserConfirmation,Long> {

    UserConfirmation findByUserAndConfirmation(User user, Confirmation confirmation);
}
