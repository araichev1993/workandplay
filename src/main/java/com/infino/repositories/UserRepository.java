package com.infino.repositories;

import com.infino.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

/**
 * Created by Aleksandar on 4/17/2017.
 */
@Repository
public interface UserRepository extends CrudRepository<User,Long> {

    User findOneByUsername(String username);
}
