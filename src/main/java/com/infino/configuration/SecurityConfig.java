package com.infino.configuration;

import com.infino.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created by Aleksandar on 4/17/2017.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;


    @Bean
    public BCryptPasswordEncoder getBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userService).passwordEncoder(getBCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/","/tether/**", "/css/**","/js/**","/bootstrap/**", "/jquery/**", "/register/**","/connect/**").permitAll()
                .antMatchers("/campaign").permitAll()
                .antMatchers("/leaderboard/**").permitAll()
                .antMatchers("/step/**").authenticated()
                .antMatchers("/userStep/**").access("hasRole('ADMIN') OR hasRole('MODERATOR')")
                .antMatchers("/campaign/**").access("hasRole('ADMIN')")
                .antMatchers("/company/**").access("hasRole('ADMIN')")
                .antMatchers("/campaign/delete/**").access("hasRole('ADMIN')")
                .antMatchers("/profile/**").access("hasRole('ADMIN') OR hasRole('USER')")
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").permitAll()
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .rememberMe()
                .rememberMeParameter("rememberMe")
                .key("GolqmaTaina")
                .tokenValiditySeconds(604800)
                .and()
                .logout().logoutSuccessUrl("/login?logout")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")).permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/unauthorized")
                .and()
                .csrf().disable();
    }
}
