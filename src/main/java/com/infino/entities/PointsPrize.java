package com.infino.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Aleksandar on 4/26/2017.
 */
@Entity
@DiscriminatorValue(value = "points")
public class PointsPrize extends Prize {

    private BigDecimal points;

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }
}
