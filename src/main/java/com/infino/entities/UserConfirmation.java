package com.infino.entities;

import javax.persistence.*;

/**
 * Created by Aleksandar on 5/3/2017.
 */
@Entity
@Table(name = "user_confirmation")
public class UserConfirmation {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "confirmation_id")
    private Confirmation confirmation;

    private String confirtmationText;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public String getConfirtmationText() {
        return confirtmationText;
    }

    public void setConfirtmationText(String confirtmationText) {
        this.confirtmationText = confirtmationText;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Confirmation getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Confirmation confirmation) {
        this.confirmation = confirmation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
