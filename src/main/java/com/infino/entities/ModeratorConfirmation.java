package com.infino.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Entity
@DiscriminatorValue("moderator")
public class ModeratorConfirmation extends Confirmation {
}
