package com.infino.entities;

import com.infino.enums.ConfirmationLevel;

import javax.persistence.*;

/**
 * Created by Aleksandar on 5/3/2017.
 */
@Entity
@Table(name = "user_step")
public class UserStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "step_id")
    private Step step;

    private Boolean isLast=false;

    @Enumerated(EnumType.STRING)
    private ConfirmationLevel confirmationLevel;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_confirmation_id")
    private UserConfirmation userConfirmation;

    public Boolean getLast() {
        return isLast;
    }

    public void setLast(Boolean last) {
        isLast = last;
    }

    public ConfirmationLevel getConfirmationLevel() {
        return confirmationLevel;
    }

    public void setConfirmationLevel(ConfirmationLevel confirmationLevel) {
        this.confirmationLevel = confirmationLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserConfirmation getUserConfirmation() {
        return userConfirmation;
    }

    public void setUserConfirmation(UserConfirmation userConfirmation) {
        this.userConfirmation = userConfirmation;
    }
}
