package com.infino.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by Aleksandar on 5/1/2017.
 */
@Entity
@DiscriminatorValue("barcode")
public class BarcodeConfirmation extends Confirmation {

    private String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
