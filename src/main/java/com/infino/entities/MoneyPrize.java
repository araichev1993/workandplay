package com.infino.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Aleksandar on 4/26/2017.
 */
@Entity
@DiscriminatorValue("money")
public class MoneyPrize extends Prize{

    private BigDecimal money;

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
