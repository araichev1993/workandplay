package com.infino.models.bindingModels;

/**
 * Created by Aleksandar on 5/4/2017.
 */
public class GameConfirmationBindingModel {

    private String confirmationText;

    private Long stepId;

    public String getConfirmationText() {
        return confirmationText;
    }

    public void setConfirmationText(String confirmationText) {
        this.confirmationText = confirmationText;
    }

    public Long getStepId() {
        return stepId;
    }

    public void setStepId(Long stepId) {
        this.stepId = stepId;
    }
}
