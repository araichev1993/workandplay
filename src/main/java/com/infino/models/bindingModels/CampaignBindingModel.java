package com.infino.models.bindingModels;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandar on 4/30/2017.
 */
public class CampaignBindingModel {

    private Boolean isMultiple;

    @NotBlank(message = "Description should not be blank")
    @Min(3)
    private String description;

    private String company;

    private Date startDate;

    private Date endDate;

    private List<StepBindingModel> steps;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<StepBindingModel> getSteps() {
        return steps;
    }

    public void setSteps(List<StepBindingModel> steps) {
        this.steps = steps;
    }

    public Boolean getMultiple() {
        return isMultiple;
    }

    public void setMultiple(Boolean multiple) {
        isMultiple = multiple;
    }
}
