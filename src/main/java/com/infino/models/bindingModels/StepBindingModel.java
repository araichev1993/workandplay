package com.infino.models.bindingModels;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Aleksandar on 4/30/2017.
 */
public class StepBindingModel {

    private String description;

    private String confirmations;

    private List<HintBindingModel> hints;

    private BigDecimal prize;

    public BigDecimal getPrize() {
        return prize;
    }

    public void setPrize(BigDecimal prize) {
        this.prize = prize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(String confirmations) {
        this.confirmations = confirmations;
    }

    public List<HintBindingModel> getHints() {
        return hints;
    }

    public void setHints(List<HintBindingModel> hints) {
        this.hints = hints;
    }
}
