package com.infino.models.bindingModels;

import com.infino.anotations.UniqueCompanyName;

import javax.validation.constraints.Size;

/**
 * Created by Aleksandar on 5/2/2017.
 */
public class CompanyBindingModel {

    @UniqueCompanyName(message = "Company already exists")
    @Size(min = 3)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
