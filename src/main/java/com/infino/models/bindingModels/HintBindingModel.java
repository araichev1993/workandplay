package com.infino.models.bindingModels;

import java.math.BigDecimal;

/**
 * Created by Aleksandar on 4/30/2017.
 */
public class HintBindingModel {

    private String description;

    private BigDecimal price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
