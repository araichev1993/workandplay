package com.infino.models.viewModels;

import java.math.BigDecimal;

/**
 * Created by Aleksandar on 5/3/2017.
 */
public class GameHintViewModel {

    private Long id;

    private String description;

    private Boolean enoughUserMoney = false;

    private BigDecimal price;

    public Boolean getEnoughUserMoney() {
        return enoughUserMoney;
    }

    public void setEnoughUserMoney(Boolean enoughUserMoney) {
        this.enoughUserMoney = enoughUserMoney;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
