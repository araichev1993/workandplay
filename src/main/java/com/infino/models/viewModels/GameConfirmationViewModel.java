package com.infino.models.viewModels;

import com.infino.enums.ConfirmationType;

/**
 * Created by Aleksandar on 5/3/2017.
 */
public class GameConfirmationViewModel {

    private Long id;

    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
