package com.infino.models.viewModels;

/**
 * Created by Aleksandar on 5/2/2017.
 */
public class ConfirmationEditViewModel {
    private Long id;

    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
