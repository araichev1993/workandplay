package com.infino.models.viewModels;

import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandar on 5/2/2017.
 */
public class CampaignEditViewModel {

    private Long id;

    private String description;

    private String company;

    private Date startDate;

    private Date endDate;

    private List<StepEditViewModel> steps;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<StepEditViewModel> getSteps() {
        return steps;
    }

    public void setSteps(List<StepEditViewModel> steps) {
        this.steps = steps;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
