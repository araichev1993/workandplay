package com.infino.models.viewModels;

/**
 * Created by Aleksandar on 5/3/2017.
 */
public class CompanyViewModel {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
