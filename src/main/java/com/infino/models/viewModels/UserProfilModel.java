package com.infino.models.viewModels;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Aleksandar on 4/27/2017.
 */
public class UserProfilModel {

    private String username;

    private String role;

    private BigDecimal sum;

    private BigDecimal points;

    private String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }
}
