package com.infino.models.viewModels;

import com.infino.enums.ConfirmationLevel;

import java.util.List;

/**
 * Created by Aleksandar on 5/3/2017.
 */
public class GameStepViewModel {

    private Long id;

    private String description;

    private ConfirmationLevel confirmationLevel = ConfirmationLevel.NEW;

    private GameConfirmationViewModel confirmation;

    public ConfirmationLevel getConfirmationLevel() {
        return confirmationLevel;
    }

    public void setConfirmationLevel(ConfirmationLevel confirmationLevel) {
        this.confirmationLevel = confirmationLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public GameConfirmationViewModel getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(GameConfirmationViewModel confirmation) {
        this.confirmation = confirmation;
    }
}
