package com.infino.models.viewModels;

import java.util.List;

/**
 * Created by Aleksandar on 4/30/2017.
 */
public class StepEditViewModel {

    private Long id;

    private String description;

    private ConfirmationEditViewModel confirmation;

    private List<HintEditViewModel> hints;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<HintEditViewModel> getHints() {
        return hints;
    }

    public void setHints(List<HintEditViewModel> hints) {
        this.hints = hints;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConfirmationEditViewModel getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(ConfirmationEditViewModel confirmation) {
        this.confirmation = confirmation;
    }
}
