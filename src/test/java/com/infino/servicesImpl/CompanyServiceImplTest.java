package com.infino.servicesImpl;

import com.infino.entities.Company;
import com.infino.repositories.CompanyRepository;
import com.infino.services.CompanyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Aleksandar on 5/9/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CompanyServiceImplTest {

    public static final String VALID_COMPANY_NAME = "MTEL";

    @Autowired
    private CompanyService companyService;
    @MockBean
    private CompanyRepository companyRepository;
    @Mock
    private Pageable pageable;


    @Test
    public void findAllCompanyNamesGivenCompanyShouldReturnValidCompanyName() throws Exception {
        Company company = new Company();
        company.setName(VALID_COMPANY_NAME);
        List<Company> companies = new ArrayList<>();
        companies.add(company);
        Mockito.when(this.companyRepository.findAll()).thenReturn(companies);

        List<String> companyNames = this.companyService.findAllCompanyNames();
        assertEquals(companyNames.get(0),VALID_COMPANY_NAME);

    }

}