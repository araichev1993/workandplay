package com.infino.servicesImpl;

import com.infino.entities.Campaign;
import com.infino.entities.Company;
import com.infino.models.bindingModels.CampaignBindingModel;
import com.infino.models.viewModels.CampaignEditViewModel;
import com.infino.repositories.CampaignRepository;
import com.infino.services.CampaignService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Aleksandar on 5/9/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CampaignServiceImplTest {


    public static final Long VALID_ID =  1l;
    public static final String VALID_DESCRIPTION =  "Campaign";

    @MockBean
    private CampaignRepository campaignRepository;

    @Autowired
    private CampaignService campaignService;


    @Test
    public void editCampaignGivenCampaignShouldReturnValidEditModel() throws Exception {
        Campaign campaign = new Campaign();
        campaign.setId(VALID_ID);
        campaign.setCompany(new Company());
        campaign.setDescription("Campaign");
        Mockito.when(campaignRepository.findOne(VALID_ID)).thenReturn(campaign);

        CampaignEditViewModel campaignEditViewModel = this.campaignService.editCampaign(VALID_ID);

        assertEquals(campaignEditViewModel.getDescription(),VALID_DESCRIPTION);

    }



}