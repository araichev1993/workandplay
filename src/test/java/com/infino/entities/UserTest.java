package com.infino.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Aleksandar on 5/8/2017.
 */
@RunWith(SpringRunner.class)
public class UserTest {

    public static final String EXPECTED_ROLE_NAME = "ROLE_USER";

    private User user;

    @Mock
    private Role role;

    @Before
    public void setUp() throws Exception {
        user = new User();
        when(this.role.getAuthority()).thenReturn("ROLE_USER");
    }

    @Test
    public void addRole() throws Exception {
        this.user.addRole(this.role);

        String actualRoleName = this.user.getAuthorities().iterator().next().getAuthority();

        assertEquals(actualRoleName,EXPECTED_ROLE_NAME);
    }

}